import json
import socket
import sys
from math import pi

class Util(object):

    @staticmethod
    def getID(carID):
        return carID['name']+carID['color']


class Track(object):
    def __init__(self, track):
        self.pieces = track['pieces']
        self.lanes = {}
        self.lanes['left'] = 0
        self.lanes['right'] = 0
        for lane in track['lanes']:
            self.lanes[lane['index']] = lane['distanceFromCenter']

        for lane in track['lanes']:
            if ( lane['distanceFromCenter'] > self.lanes[self.lanes['right']] ):
                self.lanes['right'] = lane['index']
            if ( lane['distanceFromCenter'] < self.lanes[self.lanes['left']] ):
                self.lanes['left'] = lane['index']

        self.carPositions = {}
        self.lastCarPositions = {}

        self.tweak = 1.0
        self.canTweak = True

        self.canSwitch = True

    def updatePosition(self, carPositions, myCarID):
        self.lastCarPositions = self.carPositions.copy()
        for car in carPositions:
            carID = Util.getID(car['id'])
            piecePosition = car['piecePosition']
            self.carPositions[carID] = piecePosition
            print "%s - %02d %f" % ( carID, piecePosition['pieceIndex'], piecePosition['inPieceDistance'] )

        if not self.lastCarPositions:
            self.lastCarPositions = self.carPositions.copy()

        if ( self.lastCarPositions[myCarID]['pieceIndex'] != self.carPositions[myCarID]['pieceIndex'] ):
            self.canTweak = True

        if ( self.lastCarPositions[myCarID]['lane']['endLaneIndex'] != self.lastCarPositions[myCarID]['lane']['startLaneIndex']):
            self.canSwitch = True

    def isTurn(self, pieceIndex):
        return 'angle' in self.pieces[pieceIndex]

    def isSwitch(self, pieceIndex):
        return 'switch' in self.pieces[pieceIndex]

    def getLength(self, pieceIndex, lane):
        if not self.isTurn(pieceIndex):
            return self.pieces[pieceIndex]['length']
        else:
            offsetRadius = self.lanes[lane]
            if self.pieces[pieceIndex]['angle'] > 0: # left turn
                offsetRadius = -offsetRadius
            return 2 * pi * ( self.pieces[pieceIndex]['radius'] + offsetRadius ) * abs(self.pieces[pieceIndex]['angle']) / 360

    def getSpeed(self, carID):
        lastPiece, newPiece = self.lastCarPositions[carID]['pieceIndex'], self.carPositions[carID]['pieceIndex']
        lastDistance, newDistance = self.lastCarPositions[carID]['inPieceDistance'], self.carPositions[carID]['inPieceDistance']

        # print '***'
        # print self.carPositions[carID]['lane']['endLaneIndex']
        # print lastPiece
        # print newPiece
        # print lastDistance
        # print newDistance
        # print '***'

        if ( newPiece == lastPiece ):
            return newDistance-lastDistance
        else:
            return newDistance + self.getLength(lastPiece, self.carPositions[carID]['lane']['endLaneIndex']) - lastDistance


    def getLengthToNextTurn(self, carID):
        pieceIndex = int(self.carPositions[carID]['pieceIndex'])
        lane = self.carPositions[carID]['lane']['endLaneIndex']
        lengthToTurn = self.getLength(pieceIndex, lane) - self.carPositions[carID]['inPieceDistance']
        while True:
            pieceIndex = ( pieceIndex + 1 ) % len(self.pieces)
            if ( self.isTurn(pieceIndex) ):
                break
            lengthToTurn += self.getLength(pieceIndex, lane)
        return ( pieceIndex, lengthToTurn, self.pieces[pieceIndex]['angle'] )

    def tweakTurn(self, mult):
        if ( self.canTweak ):
            self.tweak *= mult
            self.canTweak = False

    def getFullAngle(self, piece):
        if not self.isTurn(piece):
            return 1

        angle = abs(self.pieces[piece]['angle'])
        while True:
            piece = ( piece + 1 ) % len(self.pieces)
            if ( not self.isTurn(piece) ):
                break
            angle += abs(self.pieces[piece]['angle'])
        return angle

    def getSpeedForTurn(self, piece):
        print 'turnAngle: %f' % abs(self.getFullAngle(piece))
        turnLengthModifier = 0.8 + 0.15 / ( abs(self.getFullAngle(piece)) / 45.0 )
        print "turnLengthModifier: %f" % turnLengthModifier
        return self.pieces[piece]['radius'] / 17.0 * turnLengthModifier * self.tweak 

    def brakeNow(self, carID, speed):
        if ( speed == 0 ):
            return False

        pieceIndex, lengthToTurn, angle = self.getLengthToNextTurn(carID)
        targetSpeed = self.getSpeedForTurn(pieceIndex)

        deltaSpeed = targetSpeed - speed

        ticksToTurn = lengthToTurn / speed
        ticksToDecel = abs(deltaSpeed) / (0.08 + abs(speed)/200.0) / self.tweak

        print 'Next Turn in %f ( %s )' % ( lengthToTurn, 'Right' if angle > 0 else 'Left' )
        print 'TargetSpeed: %f' % targetSpeed
        print 'Delta Speed: %f' % deltaSpeed
        print 'ticksToTurn: %f' % ( ticksToTurn )
        print 'ticksToDecel: %f' % ( ticksToDecel )
        print 'tweakTurn: %f' % ( self.tweak )

        if ( deltaSpeed > 0 ):
            return False
        return ( ticksToTurn < ticksToDecel + 1 ) 

    def getSwitchDir(self, carID):
        pieceIndex = int(self.carPositions[carID]['pieceIndex'])
        goLeft = 0
        while True:
            pieceIndex = ( pieceIndex + 1 ) % len(self.pieces)
            if ( self.isSwitch(pieceIndex) ):
                break

        while True:
            pieceIndex = ( pieceIndex + 1 ) % len(self.pieces)
            if ( self.isSwitch(pieceIndex) ):
                break

            if ( self.isTurn(pieceIndex) ):
                goLeft -= self.pieces[pieceIndex]['angle']


        print '*** goLeft = %f' % goLeft

        return 'Left' if goLeft > 0 else 'Right' if goLeft < 0 else None

    def switchNow(self, carID):
        if ( not self.canSwitch ):
            return False

        pieceIndex = int(self.carPositions[carID]['pieceIndex'])
        nextPiece = ( pieceIndex + 1 ) % len(self.pieces)

        if ( not self.isSwitch(nextPiece) ):
            return False

        lane = self.carPositions[carID]['lane']['endLaneIndex']

        direction = self.getSwitchDir(carID)

        if ( direction == 'Left' ):
            return ( lane != self.lanes['left'] )
        elif ( direction == 'Right' ):
            return ( lane != self.lanes['right'] )
        else:
            return False

    def getStraightLength(self, carID):
        pieceIndex = int(self.carPositions[carID]['pieceIndex'])
        lane = self.carPositions[carID]['lane']['endLaneIndex']

        if ( self.isTurn(pieceIndex) ):
            return 0

        length = self.getLength(pieceIndex, lane) - self.carPositions[carID]['inPieceDistance']

        while True:
            pieceIndex = ( pieceIndex + 1 ) % len(self.pieces)
            if ( self.isTurn(pieceIndex) ):
                break

            length += self.pieces[pieceIndex]['length']

        return length


class Telemetry(object,):
    def __init__(self, track, myID):
        self.track = track
        self.myID = myID

        self.lastAngle = 0
        self.slipAngle = 0
        self.slipAccel = 0

        self.lastSpeed = 0
        self.speed = 0

        self.accel = 0

        self.lastThrottle = 0

        # defaultAccel = None #0.0
        # accelMap - axes: speed, throttle, slipAngle
        # self.accelMap = [ [ [ defaultAccel for slip in range(90) ] for throttle in range(10) ] for speed in range(10) ]
        # self.accelMap = [ [ defaultAccel  for throttle in range(11) ] for speed in range(200) ]

        # self.calcStat = {
        #     'accel': 1.0,
        #     'decel': 1.0,
        #     'skid': 1.0
        # }

    def guessAccel(self, throttle, speed, slip):
        return 0

    def update(self, myData):
        self.slipAngle = abs(myData['angle'])
        self.slipAccel = abs(self.slipAngle) - abs(self.lastAngle)
        self.speed = self.track.getSpeed(self.myID)
        self.accel = self.speed - self.lastSpeed

        # accelMapSpeed = int(round(self.lastSpeed,1)*10)
        # accelMapThrottle = int(round(self.lastThrottle,1)*10)
        # self.accelMap[accelMapSpeed][accelMapThrottle] = self.accel

        # print '***'
        # print self.myID
        # print 'LastSpeed: %f' % self.lastSpeed
        # print 'LastThrottle: %f' % self.lastThrottle
        # print 'Accel: %f' % self.accel
        # print "accelMap[%d][%d] = %f" % ( accelMapSpeed, accelMapThrottle, self.accel)
        # print '***'

        # outfile = open('accelMap', 'w')
        # outfile.write(repr(self.accelMap) + "\n")
        # outfile.close()

        self.lastSpeed = self.speed
        self.lastAngle = self.slipAngle

    def throttle(self, throttle):
        self.lastThrottle = throttle

    def calcThrottle(self):
        throttle = 1.0

        if self.slipAngle > 45:
            if self.slipAccel > 0:
                throttle = 0
            elif self.slipAccel > -1:
                throttle = 0.5
            elif self.slipAccel > -2:
                throttle = 0.8

        elif self.slipAngle > 30:
            if self.slipAccel > 3:
                throttle = 0
            elif self.slipAccel > 0:
                throttle = 0.7

        elif self.slipAngle > 15:
            if self.slipAccel > 6:
                throttle = 0
            elif self.slipAccel > 3:
                throttle = 0.7

        return throttle




class Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.myID = ""

        self.track = None
        self.tele = None 
        self.last_throttle = 1.0
        self.tick = 0

        self.canTurbo = False


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if 1:
            return self.msg("join", {
               "name": self.name,
               "key": self.key
            })
        else:
            return self.msg("joinRace", {
                "botId": {
                    "name": self.name,
                    "key": self.key
                },
                "trackName": "france",
                "carCount": 1
            } )

    def throttle(self, throttle):
        throttle = 1.0 if throttle > 1.0 else throttle
        self.tele.throttle(throttle)

        self.msg("throttle", throttle)
        self.last_throttle = throttle
        # print 'throttle %f'%throttle

    def ping(self):
        self.msg("ping", {})

    def useTurbo(self):
        self.canTurbo = False
        self.msg("turbo", "VROOM VROOM")

    def switch(self, direction):
        self.msg("switchLane", direction)

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_crash(self, data):
        print("on crash")
        print data
        if ( Util.getID(data) == self.myID ):
            print("it was you!")
            self.track.tweakTurn(0.9)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        print data
        self.ping()

    def on_turbo_avail(self, data):
        print "Turbo available"
        print data
        self.canTurbo = True
        self.ping()

    def on_nothing(self, data):
        print 'on_nothing'
        print data
        self.ping()

    def on_yourCar(self, data):
        print 'on_yourCar'
        print data
        self.myID = Util.getID(data)

    def on_gameInit(self, data):
        print 'on_gameInit'
        print json.dumps(data, indent=2)
        self.track = Track(data['race']['track'])
        self.tele = Telemetry( self.track, self.myID )

    def on_car_positions(self, data):
        print 'on_car_positions'
        # print data

        self.track.updatePosition(data, self.myID)

        myData = [ x for x in data if Util.getID(x['id']) == self.myID ][0]

        self.tele.update(myData)

        pieceIndex = myData['piecePosition']['pieceIndex']
        inPieceDistance = myData['piecePosition']['inPieceDistance']

        # print data
#        print 'Plan: T: %f @ %f' % ( self.plan[pieceIndex]['throttle'] ,self.plan[pieceIndex]['at'] )
#        print 'Last Throttle: %f' % self.last_throttle
        print 'SlipAngle: %f' % self.tele.slipAngle
        print 'SlipAccel: %f' % self.tele.slipAccel
        print 'Speed: %f' % self.tele.speed
        print 'Accel: %f' % self.tele.accel

        if ( self.track.switchNow(self.myID) ):
            print 'SWITCH'
            self.switch(self.track.getSwitchDir(self.myID))
            self.track.canSwitch = False
        elif ( self.track.brakeNow(self.myID, self.tele.speed) ):
            print 'BRAKE NOW'
            self.throttle( 0 )
#        if ( inPieceDistance > self.plan[pieceIndex]['at'] ):
#            self.throttle( self.plan[pieceIndex]['throttle'])
        elif ( self.tele.speed == 0 ):
            print 'NO SPEED'
            self.throttle(1.0)
        elif ( self.tele.slipAngle > 5 ):
            if ( self.tele.slipAngle > 50 ):
                self.track.tweakTurn( 0.97 )
            throt = self.tele.calcThrottle()
            print 'SLIPPING - throttle: %f' % throt
            self.throttle( throt )
        elif ( self.canTurbo and self.track.getStraightLength(self.myID) > 300 ):
            print 'TURBO'
            self.useTurbo()
        else:
            print 'FULL THROTTLE'
            self.throttle(1.0)
            if ( self.track.isTurn(pieceIndex) ):
                self.track.tweakTurn(1.005)



        self.ping()
        # print data

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_avail,
            'yourCar': self.on_yourCar,
            'gameInit': self.on_gameInit
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            self.tick = msg['gameTick'] if 'gameTick' in msg else self.tick
            # if self.tick > 3:
            #     exit()
            print '*************** GAME TICK %d ***************' % self.tick
            # print msg

            msg_type, data = msg['msgType'], msg['data']

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("*Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.run()
